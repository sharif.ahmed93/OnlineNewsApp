package com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by SHARIF on 22-Oct-17.
 */
public class RetrofitApiClient {

    private static  Retrofit retrofitInstance = null;

    /**
     * Get Retrofit Instance
     */
    public static Retrofit getRetrofitInstance(String ROOT_URL) {
        if (retrofitInstance == null){
            return new Retrofit.Builder()
                    .baseUrl(ROOT_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitInstance;
    }

}
