package com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface;

import com.example.sharif.onlinenewsapp.Model.ArticleResponse;
import com.example.sharif.onlinenewsapp.Model.Website;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by SHARIF on 20-Oct-17.
 */
public interface NewsArticleApiInterface {
    @GET
    Call<ArticleResponse> getNewsArticleResponse(@Url String url);
}
