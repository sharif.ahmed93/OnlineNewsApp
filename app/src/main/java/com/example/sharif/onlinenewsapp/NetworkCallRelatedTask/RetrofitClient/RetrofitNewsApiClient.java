package com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitClient;

import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface.NewsSourceApiInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by SHARIF on 16-Oct-17.
 */
public class RetrofitNewsApiClient {

    private static final String ROOT_URL = "https://newsapi.org/";
    private static final String API_KEY = "f034e5ffdd1d4bdb99b3e1d6e4291bf6";

    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static NewsSourceApiInterface getNewsApiInterface() {
        return getRetrofitInstance().create(NewsSourceApiInterface.class);
    }
}
