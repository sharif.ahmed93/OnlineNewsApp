package com.example.sharif.onlinenewsapp.Model;

import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.IconRetrofitPojo.Icon;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SHARIF on 17-Oct-17.
 */
public class IconModelResponse {

    private String url;
    private List<Icon> icons;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Icon> getIcons() {
        return icons;
    }

    public void setIcons(List<Icon> icons) {
        this.icons = icons;
    }
}
