package com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface;

import com.example.sharif.onlinenewsapp.Model.Website;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by SHARIF on 16-Oct-17.
 */
public interface NewsSourceApiInterface {
    @GET("v1/sources?language=en")
    Call<Website> getNewsSourceResponse();
}
