package com.example.sharif.onlinenewsapp.Activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.sharif.onlinenewsapp.Model.Article;
import com.example.sharif.onlinenewsapp.Model.ArticleResponse;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitCommon.Common;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface.NewsArticleApiInterface;
import com.example.sharif.onlinenewsapp.R;
import com.example.sharif.onlinenewsapp.RecyclerAdapter.NewsArticleListAdapter;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.github.florent37.diagonallayout.DiagonalLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsDetailsActivity extends AppCompatActivity {

    KenBurnsView kenBurnsView;
    DiagonalLayout diagonalLayout;
    AlertDialog spotsDialogue;
    NewsArticleApiInterface newsArticleApiInterface;
    TextView topArticleAuthor;
    TextView topArticleTitle;
    SwipeRefreshLayout swipeRefreshLayout;
    String source = "";
    String sortBy = "";
    String webHotUrl = "";
    NewsArticleListAdapter  newsArticleListAdapter;
    RecyclerView articleListRecyclerView;
    RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);


        newsArticleApiInterface = Common.getNewsArticleApiInterface();
        spotsDialogue = new SpotsDialog(this);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.newsDetailsSwipeRefreshId);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadNewsArticle(source,true);
            }
        });
        diagonalLayout = (DiagonalLayout) findViewById(R.id.diagonalLayoutId);
        diagonalLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Click to latest news

                Intent detailArticleIntent = new Intent(getApplicationContext(),ArticleDetailsActivity.class);
                detailArticleIntent.putExtra("webURL",webHotUrl);
                startActivity(detailArticleIntent);
            }
        });
        kenBurnsView = (KenBurnsView) findViewById(R.id.kenBurnsViewId);
        topArticleAuthor = (TextView) findViewById(R.id.topAuthorTextId);
        topArticleTitle = (TextView) findViewById(R.id.topTitleTextId);
        articleListRecyclerView = (RecyclerView) findViewById(R.id.recycleListNewsId);
        articleListRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        articleListRecyclerView.setLayoutManager(layoutManager);



        //Intent receive
        if(getIntent() != null){
            source = getIntent().getStringExtra("source");
            sortBy = getIntent().getStringExtra("sortBy");
            if(!source.isEmpty() && !sortBy.isEmpty()){
                loadNewsArticle(source,false);
            }
        }

    }

    private void loadNewsArticle(String source, boolean isRefreshed) {
        if (!isRefreshed){
            spotsDialogue.show();
            Call<ArticleResponse> call = newsArticleApiInterface.getNewsArticleResponse(Common.getApiUrl(source,sortBy,Common.API_KEY));
            call.enqueue(new Callback<ArticleResponse>() {
                @Override
                public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                    spotsDialogue.dismiss();

                    Picasso.with(getApplicationContext()).load(response.body().getArticles().get(0).getUrlToImage()).into(kenBurnsView);
                    //topArticleAuthor.setText(response.body().getArticles().get(0).getAuthor());
                    topArticleTitle.setText(response.body().getArticles().get(0).getTitle());
                    webHotUrl = response.body().getArticles().get(0).getUrl();

                    //Load remain articles.....

                    List<Article>removeFirstArticle = response.body().getArticles();
                    removeFirstArticle.remove(0);
                    newsArticleListAdapter = new NewsArticleListAdapter(removeFirstArticle,getApplicationContext());
                    newsArticleListAdapter.notifyDataSetChanged();
                    articleListRecyclerView.setAdapter(newsArticleListAdapter);


                }

                @Override
                public void onFailure(Call<ArticleResponse> call, Throwable t) {

                }
            });

        }else{
            spotsDialogue.show();
            Call<ArticleResponse> call = newsArticleApiInterface.getNewsArticleResponse(Common.getApiUrl(source,sortBy,Common.API_KEY));
            call.enqueue(new Callback<ArticleResponse>() {
                @Override
                public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                    spotsDialogue.dismiss();

                    Picasso.with(getApplicationContext()).load(response.body().getArticles().get(0).getUrlToImage()).into(kenBurnsView);
                    //topArticleAuthor.setText(response.body().getArticles().get(0).getAuthor());
                    topArticleTitle.setText(response.body().getArticles().get(0).getTitle());
                    webHotUrl = response.body().getArticles().get(0).getUrl();

                    //Load remain articles.....

                    List<Article>removeFirstArticle = response.body().getArticles();
                    removeFirstArticle.remove(0);
                    newsArticleListAdapter = new NewsArticleListAdapter(removeFirstArticle,getApplicationContext());
                    newsArticleListAdapter.notifyDataSetChanged();
                    articleListRecyclerView.setAdapter(newsArticleListAdapter);


                }

                @Override
                public void onFailure(Call<ArticleResponse> call, Throwable t) {

                }
            });
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
