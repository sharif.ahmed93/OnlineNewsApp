package com.example.sharif.onlinenewsapp.RecyclerAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sharif.onlinenewsapp.Activity.ArticleDetailsActivity;
import com.example.sharif.onlinenewsapp.Model.Article;
import com.example.sharif.onlinenewsapp.Model.ISO8601DateParser;
import com.example.sharif.onlinenewsapp.R;
import com.example.sharif.onlinenewsapp.RecyclerClickInterface.RecyclerItemClickListener;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by SHARIF on 21-Oct-17.
 */
public class NewsArticleListAdapter extends RecyclerView.Adapter<NewsArticleListAdapter.NewsArticleListViewHolder> {

    List<Article>articleArrayList;
    Context context;

    public NewsArticleListAdapter(List<Article> articleArrayList, Context context) {
        this.articleArrayList = articleArrayList;
        this.context = context;
    }

    @Override
    public NewsArticleListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_news_article_layout,parent,false);
        NewsArticleListViewHolder newsArticleListViewHolder = new NewsArticleListViewHolder(view);
        return newsArticleListViewHolder;
    }

    @Override
    public void onBindViewHolder(NewsArticleListViewHolder holder, final int position) {
        Picasso.with(context).load(articleArrayList.get(position).getUrlToImage()).into(holder.articleCircleImageView);

        if(articleArrayList.get(position).getTitle().length()>65){
            holder.articleTitleTextView.setText(articleArrayList.get(position).getTitle().substring(0,65)+".........");
        }else {
            holder.articleTitleTextView.setText(articleArrayList.get(position).getTitle());
        }

        Date date =null;

        try {
            date = ISO8601DateParser.parse(articleArrayList.get(position).getPublishedAt());
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
        holder.articleTimeView.setReferenceTime(date.getTime());
        //Set Event Click
        holder.setRecyclerItemClickListener(new RecyclerItemClickListener() {
            @Override
            public void onClick(View view, int itemPosition, boolean isLongClick) {
                Intent detailArticleIntent = new Intent(context,ArticleDetailsActivity.class);
                detailArticleIntent.putExtra("webURL",articleArrayList.get(position).getUrl());
                detailArticleIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(detailArticleIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return articleArrayList.size();
    }

    public static class NewsArticleListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private RecyclerItemClickListener recyclerItemClickListener;

        public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
            this.recyclerItemClickListener = recyclerItemClickListener;
        }

        public CircleImageView articleCircleImageView;
        public TextView articleTitleTextView;
        RelativeTimeTextView articleTimeView;

        public NewsArticleListViewHolder(View itemView) {
            super(itemView);

            articleCircleImageView = (CircleImageView) itemView.findViewById(R.id.articleCircleImageId);
            articleTitleTextView = (TextView) itemView.findViewById(R.id.articleTitleTextId);
            articleTimeView = (RelativeTimeTextView) itemView.findViewById(R.id.articleTimeTextViewId);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            recyclerItemClickListener.onClick(view,getAdapterPosition(),false);
        }
    }
}
