package com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface;

import com.example.sharif.onlinenewsapp.Model.IconModelResponse;
import com.example.sharif.onlinenewsapp.Model.Website;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.IconRetrofitPojo.IconResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by SHARIF on 17-Oct-17.
 */
public interface IconApiInterface {
    @GET
    Call<IconModelResponse> getIconResponse(@Url String url);
}
