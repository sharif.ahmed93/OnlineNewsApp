package com.example.sharif.onlinenewsapp.RecyclerClickInterface;

import android.view.View;

/**
 * Created by SHARIF on 17-Oct-17.
 */
public interface RecyclerItemClickListener {
    void onClick(View view,int itemPosition,boolean isLongClick);
}
