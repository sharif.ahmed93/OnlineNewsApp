package com.example.sharif.onlinenewsapp.RecyclerAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sharif.onlinenewsapp.Activity.NewsDetailsActivity;
import com.example.sharif.onlinenewsapp.Model.IconModelResponse;
import com.example.sharif.onlinenewsapp.Model.Website;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitClient.RetrofitIconApiClient;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface.IconApiInterface;
import com.example.sharif.onlinenewsapp.R;
import com.example.sharif.onlinenewsapp.RecyclerClickInterface.RecyclerItemClickListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SHARIF on 17-Oct-17.
 */
public class NewsSourceListAdapter extends RecyclerView.Adapter<NewsSourceListAdapter.RecyclerViewHolder> {


    private Context context;
    private Website website;

    IconApiInterface iconApiInterface;
    public NewsSourceListAdapter(Context context, Website website) {
        this.context = context;
        this.website = website;

        iconApiInterface = RetrofitIconApiClient.getIconApiInterface();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_news_source_layout,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        final StringBuilder iconApi = new StringBuilder(" https://icons.better-idea.org/allicons.json?url=");
        iconApi.append(website.getSources().get(position).getUrl());
        Call<IconModelResponse> call = iconApiInterface.getIconResponse(iconApi.toString());
        call.enqueue(new Callback<IconModelResponse>() {
            @Override
            public void onResponse(Call<IconModelResponse> call, Response<IconModelResponse> response) {
                try {
                    if(response.body().getIcons().size()>0){
                        Picasso.with(context).load(response.body().getIcons().get(0).getUrl()).into(holder.circleImageView);
                    }
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<IconModelResponse> call, Throwable t) {

            }
        });
        holder.titleTextView.setText(website.getSources().get(position).getName());
        holder.setRecyclerItemClickListener(new RecyclerItemClickListener() {
            @Override
            public void onClick(View view, int itemPosition, boolean isLongClick) {
                Intent newsDetailsIntent = new Intent(context, NewsDetailsActivity.class);
                newsDetailsIntent.putExtra("source",website.getSources().get(position).getId());
                newsDetailsIntent.putExtra("sortBy",website.getSources().get(position).getSortBysAvailable().get(0));
                newsDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(newsDetailsIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return website.getSources().size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RecyclerItemClickListener recyclerItemClickListener;

        public CircleImageView circleImageView;
        public TextView titleTextView;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageId);
            titleTextView = (TextView) itemView.findViewById(R.id.sourceNameId);

            itemView.setOnClickListener(this);

        }

        public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
            this.recyclerItemClickListener = recyclerItemClickListener;
        }


        @Override
        public void onClick(View view) {
            recyclerItemClickListener.onClick(view,getAdapterPosition(),false);
        }
    }
}
