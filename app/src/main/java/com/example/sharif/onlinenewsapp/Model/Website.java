package com.example.sharif.onlinenewsapp.Model;

import java.util.List;

/**
 * Created by SHARIF on 16-Oct-17.
 */
public class Website {
    private String status;
    private List<Source> sources;

    public Website() {

    }

    public Website(String status, List<Source> sources) {
        this.status = status;
        this.sources = sources;
    }

    public List<Source> getSources() {
        return sources;
    }

    public void setSources(List<Source> sources) {
        this.sources = sources;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
