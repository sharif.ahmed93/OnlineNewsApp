package com.example.sharif.onlinenewsapp.Activity;

import android.app.AlertDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.sharif.onlinenewsapp.Model.Website;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitClient.RetrofitNewsApiClient;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface.NewsSourceApiInterface;
import com.example.sharif.onlinenewsapp.R;
import com.example.sharif.onlinenewsapp.RecyclerAdapter.NewsSourceListAdapter;
import com.google.gson.Gson;

import dmax.dialog.SpotsDialog;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    RecyclerView listNewsSite;
    RecyclerView.LayoutManager layoutManager;
    NewsSourceApiInterface newsSourceApiInterface;
    NewsSourceListAdapter newsSourceListAdapter;
    AlertDialog spotsDialog;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        newsSourceApiInterface = RetrofitNewsApiClient.getNewsApiInterface();

        listNewsSite = (RecyclerView) findViewById(R.id.sourceRecyclerId);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshId);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadJsonData(true);
            }
        });
        listNewsSite.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        listNewsSite.setLayoutManager(layoutManager);


        Paper.init(this);//Init cache

        spotsDialog = new SpotsDialog(this);

        loadJsonData(false);


    }

    private void loadJsonData(boolean isRefreshed) {

        if(!isRefreshed){
            String cache = Paper.book().read("cache");
            if(cache !=null && !cache.isEmpty())//If have cache
            {
                Website website = new Gson().fromJson(cache,Website.class);
                newsSourceListAdapter = new NewsSourceListAdapter(this,website);
                newsSourceListAdapter.notifyDataSetChanged();
                listNewsSite.setAdapter(newsSourceListAdapter);
            }
            else // If have not any cache
            {
                spotsDialog.show();
                Call<Website> call = newsSourceApiInterface.getNewsSourceResponse();
                call.enqueue(new Callback<Website>() {
                    @Override
                    public void onResponse(Call<Website> call, Response<Website> response) {
                        if(response.isSuccessful()){
                            Website website = response.body();
                            newsSourceListAdapter = new NewsSourceListAdapter(getApplicationContext(),website);
                            newsSourceListAdapter.notifyDataSetChanged();
                            listNewsSite.setAdapter(newsSourceListAdapter);
                            Paper.book().write("cache",new Gson().toJson(website));//Save to cache
                            spotsDialog.dismiss();

                        }else{
                            Toast.makeText(MainActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Website> call, Throwable t) {

                    }
                });
            }

        }
        else //If from  swipe to refresh
        {
            spotsDialog.show();
            Call<Website> call = newsSourceApiInterface.getNewsSourceResponse();
            call.enqueue(new Callback<Website>() {
                @Override
                public void onResponse(Call<Website> call, Response<Website> response) {
                    if(response.isSuccessful()){
                        Website website = response.body();
                        newsSourceListAdapter = new NewsSourceListAdapter(getApplicationContext(),website);
                        newsSourceListAdapter.notifyDataSetChanged();
                        listNewsSite.setAdapter(newsSourceListAdapter);
                        Paper.book().write("cache",new Gson().toJson(website));//Save to cache
                        swipeRefreshLayout.setRefreshing(false);
                        spotsDialog.dismiss();
                    }else{
                        Toast.makeText(MainActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Website> call, Throwable t) {

                }
            });
        }

    }
}
