package com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitClient;

import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface.IconApiInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by SHARIF on 17-Oct-17.
 */
public class RetrofitIconApiClient {

    private static final String ROOT_URL = "https://icons.better-idea.org/";

    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static IconApiInterface getIconApiInterface() {
        return getRetrofitInstance().create(IconApiInterface.class);
    }
}
