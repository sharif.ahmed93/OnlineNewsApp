package com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitCommon;

import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitClient.RetrofitApiClient;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface.IconApiInterface;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface.NewsArticleApiInterface;
import com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.RetrofitInterface.NewsSourceApiInterface;

/**
 * Created by SHARIF on 23-Oct-17.
 */
public class Common {
    public static final String ROOT_URL = "https://newsapi.org/";
    public static final String API_KEY = "f034e5ffdd1d4bdb99b3e1d6e4291bf6";

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static NewsSourceApiInterface getNewsSourceApiInterface() {
        return RetrofitApiClient.getRetrofitInstance(ROOT_URL).create(NewsSourceApiInterface.class);
    }
    public static IconApiInterface getIconApiInterface() {
        return RetrofitApiClient.getRetrofitInstance(ROOT_URL).create(IconApiInterface.class);
    }
    public static NewsArticleApiInterface getNewsArticleApiInterface() {
        return RetrofitApiClient.getRetrofitInstance(ROOT_URL).create(NewsArticleApiInterface.class);
    }
    public static String getApiUrl(String source, String sortBy,String apiKey){
        StringBuilder apiBuilder = new StringBuilder("https://newsapi.org/v1/articles?source=");
        return apiBuilder.append(source)
                .append("&sortBy=")
                .append(sortBy)
                .append("&apiKey=")
                .append(apiKey)
                .toString();
    }

}
