package com.example.sharif.onlinenewsapp.NetworkCallRelatedTask.IconRetrofitPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SHARIF on 17-Oct-17.
 */
public class IconResponse {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("icons")
    @Expose
    private List<Icon> icons = null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Icon> getIcons() {
        return icons;
    }

    public void setIcons(List<Icon> icons) {
        this.icons = icons;
    }

}
